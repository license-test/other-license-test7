# Used third party content

## Material Icons

-   Files: `schwalbe/static/lib/material-icons-*`
    -   URL: https://github.com/google/material-design-icons
    -   Author: Google
    -   License: [Apache License 2.0](https://github.com/google/material-design-icons/blob/master/LICENSE)
-   File: `schwalbe/static/lib/material-icons-*/css/material-icons.css`
    -   Derived (with modifications) from URL: https://developers.google.com/fonts/docs/material_icons?hl=en#setup_method_2_self_hosting
    -   Author: Google
    -   License: [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)
-   File: `schwalbe/static/img/favicon_poll_green.svg`
    -   Derived (with modifications) from icon "poll", URL: https://fonts.google.com/icons?selected=Material%20Icons%3Apoll%3A
    -   Author: Google
    -   License: [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)
